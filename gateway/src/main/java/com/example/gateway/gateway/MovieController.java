package com.example.gateway.gateway;

import com.example.gateway.gateway.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MovieController {

    private MovieServiceConsumer consumer;

    @Autowired
    public MovieController(MovieServiceConsumer consumer) {
        this.consumer = consumer;
    }

    @GetMapping("/movies")
    public ResponseEntity<List<Movie>> getMovies() {

        List<Movie> movies = consumer.getMovies();
        consumer.closeChannel();
        return ResponseEntity.ok(movies);
    }
}
