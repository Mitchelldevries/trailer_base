package com.example.gateway.gateway;

import com.example.trailerbase.Movie;
import com.example.trailerbase.MovieRequest;
import com.example.trailerbase.MovieResponse;
import com.example.trailerbase.MovieServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.example.trailerbase.MovieServiceGrpc.newBlockingStub;

@Service
public class MovieServiceConsumer {

    private ManagedChannel channel;

    public List<com.example.gateway.gateway.Movie> getMovies() {
        List<com.example.gateway.gateway.Movie> movies = new ArrayList<>();

        channel = ManagedChannelBuilder
                .forAddress("localhost", 8082)
                .usePlaintext(true)
                .build();

        MovieServiceGrpc.MovieServiceBlockingStub blockingStub = newBlockingStub(channel);
        MovieRequest request = MovieRequest.newBuilder().build();
        MovieResponse response = blockingStub.getMovies(request);

        transferObject(movies, response);

        return movies;
    }

    private void transferObject(List<com.example.gateway.gateway.Movie> movies, MovieResponse response) {
        response.getMoviesList().stream()
                .map(m ->
                        new com.example.gateway.gateway.Movie(m.getTitle(),
                                m.getUrl()))
                .forEach(movies::add);
    }

    public void closeChannel() {
        try {
            channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
