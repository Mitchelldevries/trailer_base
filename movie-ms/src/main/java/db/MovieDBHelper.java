package db;

import com.example.trailerbase.Movie;

import java.util.List;

public interface MovieDBHelper {

    void close();

    void create(Movie movie);

    List<Movie> findAll();
}
