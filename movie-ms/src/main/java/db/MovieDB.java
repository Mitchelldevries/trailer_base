package db;

import com.example.trailerbase.Movie;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public enum MovieDB implements MovieDBHelper {
    INSTANCE;

    // JDBC Driver name and Database URL.
    private static final String JDBC_DRIVER = "org.h2.Driver";

    // Database is in-memory and the content stays alive
    // as long as the virtual machine is running,
    private static final String DB_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";

    // H2 Database Credentials.
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";

    private Connection connection;
    private Statement statement;

    /**
     * Constructor that opens a database connection and creates a Movie table.
     */
    MovieDB() {
        try {
            Class.forName(JDBC_DRIVER);

            // Open connection with database.
            System.out.println("Connecting to database...");
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
            System.out.println("Connected database successfully...");

            // Create Movie Table.
            System.out.println("Creating table in given database...");
            statement = connection.createStatement();
            String createTableQuery = "CREATE TABLE Movies " +
                    "(title VARCHAR(255), " +
                    " url VARCHAR(255), " +
                    " PRIMARY KEY ( title ))";
            statement.execute(createTableQuery);
            System.out.println("Created MOVIES table...");

        } catch (SQLException | ClassNotFoundException e) {
            System.err.println("Cannot connect to database...");
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(Movie movie) {
        try {
            String sql = "INSERT INTO Movies " + "VALUES ('" + movie.getTitle() + "','" + movie.getUrl() + "')";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Movie> findAll() {
        List<Movie> movies = new ArrayList<>();
        try {

            String sql = "SELECT * FROM Movies";
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                movies.add(Movie.newBuilder()
                        .setTitle(rs.getString("title"))
                        .setUrl(rs.getString("url"))
                        .build());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return movies;
    }

}
