package movie_ms;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;
import java.util.logging.Logger;

public class MovieServer {

    private static final int PORT = 8082;
    private static final Logger LOG = Logger.getLogger(MovieServer.class.getName());

    public static void main(String[] args) {
        Server server = ServerBuilder.forPort(PORT)
                .addService(new MovieServiceImpl())
                .build();

        try {
            server.start();
            LOG.info("Server started on port: " + PORT);
            server.awaitTermination();
        } catch (IOException | InterruptedException e) {
            LOG.info(e.getMessage());
        }

    }
}
