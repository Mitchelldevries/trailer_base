package movie_ms;

import com.example.trailerbase.Movie;
import com.example.trailerbase.MovieRequest;
import com.example.trailerbase.MovieResponse;
import com.example.trailerbase.MovieServiceGrpc;
import db.MovieDB;
import io.grpc.stub.StreamObserver;

public class MovieServiceImpl extends MovieServiceGrpc.MovieServiceImplBase {

    private static final int NUMBER_OF_MOVIES = 5;
    private MovieDB movieDB;

    public MovieServiceImpl() {
        movieDB = MovieDB.INSTANCE;
        createMovies();
    }

    private void createMovies() {
        for (int i = 0; i < NUMBER_OF_MOVIES; i++) {
            movieDB.create(Movie.newBuilder()
                    .setTitle("Movie #" + i)
                    .setUrl("www.youtube.com")
                    .build());
        }
    }

    @Override
    public void getMovies(MovieRequest request, StreamObserver<MovieResponse> responseObserver) {
        MovieResponse response = MovieResponse.newBuilder().addAllMovies(movieDB.findAll()).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
        movieDB.close();
    }
}
