// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: trailer_base.proto

package com.example.trailerbase;

public interface MovieResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:MovieResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated .Movie movies = 1;</code>
   */
  java.util.List<com.example.trailerbase.Movie> 
      getMoviesList();
  /**
   * <code>repeated .Movie movies = 1;</code>
   */
  com.example.trailerbase.Movie getMovies(int index);
  /**
   * <code>repeated .Movie movies = 1;</code>
   */
  int getMoviesCount();
  /**
   * <code>repeated .Movie movies = 1;</code>
   */
  java.util.List<? extends com.example.trailerbase.MovieOrBuilder> 
      getMoviesOrBuilderList();
  /**
   * <code>repeated .Movie movies = 1;</code>
   */
  com.example.trailerbase.MovieOrBuilder getMoviesOrBuilder(
      int index);
}
